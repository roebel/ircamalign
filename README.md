ircamAlign
=====================

Description
--------------

ircamAlign is a tool for aligning text and audio. The present version supports alignment between texts written in French and English language 
using UTF8 encoding and the corresponding audio files in wav format.


Installation
---------------

Please see installation instructions and download links on the [wiki](https://git.forum.ircam.fr/roebel/ircamalign/wikis/home)


Releases 
--------------


IrcamAlign Version 1.2.4 (19.05.2024)
------------------------

**Changes**

-- Fixed sources for compilation on MacOSX ARM64.

IrcamAlign Version 1.2.3 (7.12.2022)
------------------------

**Changes**

-- Added support for aligning texts in English (thanks to festvox/flite phonemizer from https://github.com/festvox/flite)


IrcamAlign Version 1.1.12 (30.11.2021)
------------------------


**Changes**

- work around a few bugs with missing phonemes in lia_text2phon output
- properly read files containing UTF8 with BOM header (written by Windows)  

IrcamAlign Version 1.1.9 (22.5.2021)
------------------------


**Changes**

- switched default architecture for MacOS from i386 to nothing. So the installation scripts should now use the architecture you use on your system.  
- added support for zsh the default shell on recent MacOS versions (thanks to Frederic Cornu).

IrcamAlign Version 1.1.8 (20.5.2021)
------------------------

**Changes**

- fixed a bug in ircamAlignInstaller script that would fail when rerunning he installer after a first trial that failed.


IrcamAlign Version 1.1.7 (18.5.2021)
-------------------------

**Changes**

- more robust handling of input files that are not in latin1 encoding
- installer now properly reports failure when installation fails.
- new output flag for lia_text2xsampa that now supports storing
  the XSAMPA phoneme sequence in a text file. 


Acknowledgements
-------------------

ircamAlign is built on top of HTK  3.4.1 (see: http://htk.eng.cam.ac.uk) and LIA_PHON (https://pageperso.lis-lab.fr/~frederic.bechet/download.html)
(by  Frédéric Béchet). English phonemization is performed by means of t2p from festvox/flite (https://github.com/festvox/flite)
